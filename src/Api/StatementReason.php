<?php

namespace LogisticsX\Finances\Api;

use LogisticsX\Finances\Model\StatementReason\StatementReason\Read;
use LogisticsX\Finances\Model\StatementReason\StatementReason\Update;
use LogisticsX\Finances\Model\StatementReason\StatementReason\Write;

class StatementReason extends AbstractAPI
{
    /**
     * Retrieves the collection of StatementReason resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'code'	string
     *                       'description'	string
     *                       'status'	string
     *                       'status[]'	array
     *                       'order[code]'	string
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getStatementReasonCollection',
        'GET',
        'api/finances/statement_reasons',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a StatementReason resource.
     *
     * @param Write $Model The new StatementReason resource
     *
     * @return Read
     */
    public function postCollection(Write $Model): Read
    {
        return $this->request(
        'postStatementReasonCollection',
        'POST',
        'api/finances/statement_reasons',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a StatementReason resource.
     *
     * @param string $code Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $code): ?Read
    {
        return $this->request(
        'getStatementReasonItem',
        'GET',
        "api/finances/statement_reasons/$code",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the StatementReason resource.
     *
     * @param string $code  Resource identifier
     * @param Update $Model The updated StatementReason resource
     *
     * @return Read
     */
    public function putItem(string $code, Update $Model): Read
    {
        return $this->request(
        'putStatementReasonItem',
        'PUT',
        "api/finances/statement_reasons/$code",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the StatementReason resource.
     *
     * @param string $code Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $code): mixed
    {
        return $this->request(
        'deleteStatementReasonItem',
        'DELETE',
        "api/finances/statement_reasons/$code",
        null,
        [],
        []
        );
    }
}
