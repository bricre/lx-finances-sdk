<?php

namespace LogisticsX\Finances\Api;

use LogisticsX\Finances\Model\Currency\Currency\Read;
use LogisticsX\Finances\Model\Currency\Currency\Write;

class Currency extends AbstractAPI
{
    /**
     * Retrieves the collection of Currency resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'code'	string
     *                       'code[]'	array
     *                       'name'	string
     *                       'name[]'	array
     *                       'symbol'	string
     *                       'symbol[]'	array
     *                       'htmlCode'	string
     *                       'htmlCode[]'	array
     *                       'order[code]'	string
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getCurrencyCollection',
        'GET',
        'api/finances/currencies',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a Currency resource.
     *
     * @param Write $Model The new Currency resource
     *
     * @return Read
     */
    public function postCollection(Write $Model): Read
    {
        return $this->request(
        'postCurrencyCollection',
        'POST',
        'api/finances/currencies',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a Currency resource.
     *
     * @param string $code Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $code): ?Read
    {
        return $this->request(
        'getCurrencyItem',
        'GET',
        "api/finances/currencies/$code",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the Currency resource.
     *
     * @param string $code  Resource identifier
     * @param Write  $Model The updated Currency resource
     *
     * @return Read
     */
    public function putItem(string $code, Write $Model): Read
    {
        return $this->request(
        'putCurrencyItem',
        'PUT',
        "api/finances/currencies/$code",
        $Model->getArrayCopy(),
        [],
        []
        );
    }
}
