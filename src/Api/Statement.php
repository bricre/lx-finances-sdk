<?php

namespace LogisticsX\Finances\Api;

use LogisticsX\Finances\Model\Statement\Statement\Read;
use LogisticsX\Finances\Model\Statement\StatementInput\Statement\Write;

class Statement extends AbstractAPI
{
    /**
     * Retrieves the collection of Statement resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'id'	integer
     *                       'id[]'	array
     *                       'account'	string
     *                       'account[]'	array
     *                       'account.clientCode'	string
     *                       'account.clientCode[]'	array
     *                       'account.currencyCode'	string
     *                       'account.currencyCode[]'	array
     *                       'amount'	string
     *                       'amount[]'	array
     *                       'recordType'	string
     *                       'recordType[]'	array
     *                       'recordId'	string
     *                       'recordId[]'	array
     *                       'reason'	string
     *                       'reason[]'	array
     *                       'type'	string
     *                       'type[]'	array
     *                       'order[id]'	string
     *                       'order[createTime]'	string
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getStatementCollection',
        'GET',
        'api/finances/statements',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a Statement resource.
     *
     * @param Write $Model The new Statement resource
     *
     * @return Read
     */
    public function postCollection(Write $Model): Read
    {
        return $this->request(
        'postStatementCollection',
        'POST',
        'api/finances/statements',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a Statement resource.
     *
     * @param string $id Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $id): ?Read
    {
        return $this->request(
        'getStatementItem',
        'GET',
        "api/finances/statements/$id",
        null,
        [],
        []
        );
    }
}
