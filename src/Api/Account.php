<?php

namespace LogisticsX\Finances\Api;

use LogisticsX\Finances\Model\Account\Account\Read;
use LogisticsX\Finances\Model\Account\Account\Write;

class Account extends AbstractAPI
{
    /**
     * Retrieves the collection of Account resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'currency'	string
     *                       'currency[]'	array
     *                       'clientId'	integer
     *                       'clientId[]'	array
     *                       'clientCode'	string
     *                       'clientCode[]'	array
     *                       'balance'	string
     *                       'balance[]'	array
     *                       'order[id]'	string
     *                       'order[updateTime]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getAccountCollection',
        'GET',
        'api/finances/accounts',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a Account resource.
     *
     * @param Write $Model The new Account resource
     *
     * @return Read
     */
    public function postCollection(Write $Model): Read
    {
        return $this->request(
        'postAccountCollection',
        'POST',
        'api/finances/accounts',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a Account resource.
     *
     * @param string $id Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $id): ?Read
    {
        return $this->request(
        'getAccountItem',
        'GET',
        "api/finances/accounts/$id",
        null,
        [],
        []
        );
    }
}
