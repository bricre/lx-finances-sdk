<?php

namespace LogisticsX\Finances\Api;

use OpenAPI\Runtime\APIInterface as BaseClass;

interface APIInterface extends BaseClass
{
}
