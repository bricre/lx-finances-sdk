<?php

namespace LogisticsX\Finances\Api;

use LogisticsX\Finances\Model\StatementDetail as StatementDetailModel;
use LogisticsX\Finances\Model\StatementDetail\StatementDetail\Read;

class StatementDetail extends AbstractAPI
{
    /**
     * Retrieves the collection of StatementDetail resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'statement'	string
     *                       'statement[]'	array
     *                       'note'	string
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getStatementDetailCollection',
        'GET',
        'api/finances/statement_details',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a StatementDetail resource.
     *
     * @param StatementDetailModel $Model The new StatementDetail resource
     *
     * @return Read
     */
    public function postCollection(StatementDetailModel $Model): Read
    {
        return $this->request(
        'postStatementDetailCollection',
        'POST',
        'api/finances/statement_details',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a StatementDetail resource.
     *
     * @param string $statement Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $statement): ?Read
    {
        return $this->request(
        'getStatementDetailItem',
        'GET',
        "api/finances/statement_details/$statement",
        null,
        [],
        []
        );
    }

    /**
     * Retrieves a Statement resource.
     *
     * @param string $id Statement identifier
     *
     * @return Read|null
     */
    public function api_statements_statement_detail_get_subresourceStatementSubresource(string $id): ?Read
    {
        return $this->request(
        'api_statements_statement_detail_get_subresourceStatementSubresource',
        'GET',
        "api/finances/statements/$id/statement_detail",
        null,
        [],
        []
        );
    }
}
