<?php

namespace LogisticsX\Finances\Api;

use LogisticsX\Finances\Model\User as UserModel;

class User extends AbstractAPI
{
    /**
     * Retrieves the collection of User resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *
     * @return UserModel[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getUserCollection',
        'GET',
        'api/finances/users',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a User resource.
     *
     * @param UserModel $Model The new User resource
     *
     * @return UserModel
     */
    public function postCollection(UserModel $Model): UserModel
    {
        return $this->request(
        'postUserCollection',
        'POST',
        'api/finances/users',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a User resource.
     *
     * @param string $id Resource identifier
     *
     * @return UserModel|null
     */
    public function getItem(string $id): ?UserModel
    {
        return $this->request(
        'getUserItem',
        'GET',
        "api/finances/users/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the User resource.
     *
     * @param string    $id    Resource identifier
     * @param UserModel $Model The updated User resource
     *
     * @return UserModel
     */
    public function putItem(string $id, UserModel $Model): UserModel
    {
        return $this->request(
        'putUserItem',
        'PUT',
        "api/finances/users/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the User resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteUserItem',
        'DELETE',
        "api/finances/users/$id",
        null,
        [],
        []
        );
    }

    /**
     * Updates the User resource.
     *
     * @param string $id Resource identifier
     *
     * @return UserModel
     */
    public function patchItem(string $id): UserModel
    {
        return $this->request(
        'patchUserItem',
        'PATCH',
        "api/finances/users/$id",
        null,
        [],
        []
        );
    }
}
