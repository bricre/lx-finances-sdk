<?php

namespace LogisticsX\Finances\Model;

use OpenAPI\Runtime\AbstractModel;

/**
 * StatementDetail.
 */
class StatementDetail extends AbstractModel
{
    /**
     * @var string
     */
    public $statement = null;

    /**
     * @var string|null
     */
    public $note = null;
}
