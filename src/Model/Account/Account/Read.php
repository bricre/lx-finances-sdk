<?php

namespace LogisticsX\Finances\Model\Account\Account;

use OpenAPI\Runtime\AbstractModel;

/**
 * Account.
 */
class Read extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var string
     */
    public $clientCode = null;

    /**
     * @var string
     */
    public $balance = null;

    /**
     * @var string
     */
    public $updateTime = 'current_timestamp()';

    public $currency = null;
}
