<?php

namespace LogisticsX\Finances\Model\Account\Account;

use OpenAPI\Runtime\AbstractModel;

/**
 * Account.
 */
class Write extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var string
     */
    public $clientCode = null;

    /**
     * @var string
     */
    public $balance = null;

    /**
     * @var string|null
     */
    public $currency = null;
}
