<?php

namespace LogisticsX\Finances\Model\Account\Statement;

use OpenAPI\Runtime\AbstractModel;

/**
 * Account.
 */
class Read extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var int
     */
    public $clientId = null;

    /**
     * @var string
     */
    public $clientCode = null;

    public $currency = null;
}
