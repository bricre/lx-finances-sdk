<?php

namespace LogisticsX\Finances\Model\StatementDetail\Statement;

use OpenAPI\Runtime\AbstractModel;

/**
 * StatementDetail.
 */
class Read extends AbstractModel
{
    /**
     * @var string|null
     */
    public $note = null;
}
