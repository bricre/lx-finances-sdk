<?php

namespace LogisticsX\Finances\Model\User\Statement;

use OpenAPI\Runtime\AbstractModel;

/**
 * User.
 */
class Read extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $nickname = null;

    /**
     * @var string
     */
    public $username = null;
}
