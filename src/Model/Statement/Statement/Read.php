<?php

namespace LogisticsX\Finances\Model\Statement\Statement;

use OpenAPI\Runtime\AbstractModel;

/**
 * Statement.
 */
class Read extends AbstractModel
{
    /**
     * @var string
     */
    public $id = null;

    /**
     * @var string
     */
    public $amount = null;

    /**
     * @var string|null
     */
    public $recordType = null;

    /**
     * @var string|null
     */
    public $recordId = null;

    public $user = null;

    public $reason = null;

    /**
     * @var string
     */
    public $balanceAfter = null;

    /**
     * @var string|null
     */
    public $type = null;

    /**
     * @var string|null
     */
    public $createTime = null;

    public $account = null;

    public $statementDetail = null;
}
