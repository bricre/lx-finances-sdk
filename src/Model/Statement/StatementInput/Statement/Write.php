<?php

namespace LogisticsX\Finances\Model\Statement\StatementInput\Statement;

use OpenAPI\Runtime\AbstractModel;

/**
 * Statement.
 */
class Write extends AbstractModel
{
    /**
     * @var string|null
     */
    public $account = null;

    /**
     * @var int|null
     */
    public $clientId = null;

    /**
     * @var int|null
     */
    public $userId = null;

    /**
     * @var float
     */
    public $amount = null;

    /**
     * @var string|null
     */
    public $recordType = null;

    /**
     * @var string|null
     */
    public $recordId = null;

    /**
     * @var string|null
     */
    public $reason = null;

    /**
     * @var string|null
     */
    public $type = null;

    /**
     * @var string|null
     */
    public $note = null;
}
