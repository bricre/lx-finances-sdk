<?php

namespace LogisticsX\Finances\Model\Currency\Account;

use OpenAPI\Runtime\AbstractModel;

/**
 * Currency.
 */
class Read extends AbstractModel
{
    /**
     * @var string
     */
    public $code = null;

    /**
     * @var string|null
     */
    public $name = null;

    /**
     * @var string
     */
    public $symbol = null;

    /**
     * @var string|null
     */
    public $htmlCode = null;
}
