<?php

namespace LogisticsX\Finances\Model\StatementReason\Statement;

use OpenAPI\Runtime\AbstractModel;

/**
 * StatementReason.
 */
class Read extends AbstractModel
{
    /**
     * @var string|null
     */
    public $description = null;

    /**
     * @var string
     */
    public $status = 'ACTIVE';
}
