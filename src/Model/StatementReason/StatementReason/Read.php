<?php

namespace LogisticsX\Finances\Model\StatementReason\StatementReason;

use OpenAPI\Runtime\AbstractModel;

/**
 * StatementReason.
 */
class Read extends AbstractModel
{
    /**
     * @var string
     */
    public $code = null;

    /**
     * @var string|null
     */
    public $description = null;

    /**
     * @var string
     */
    public $status = 'ACTIVE';
}
