<?php

namespace LogisticsX\Finances\Model\StatementReason\StatementReason;

use OpenAPI\Runtime\AbstractModel;

/**
 * StatementReason.
 */
class Update extends AbstractModel
{
    /**
     * @var string|null
     */
    public $description = null;

    /**
     * @var string
     */
    public $status = 'ACTIVE';
}
