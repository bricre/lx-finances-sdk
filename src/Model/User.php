<?php

namespace LogisticsX\Finances\Model;

use OpenAPI\Runtime\AbstractModel;

/**
 * User.
 */
class User extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $nickname = null;

    /**
     * @var string
     */
    public $username = null;
}
