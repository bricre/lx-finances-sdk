<?php

namespace LogisticsX\Finances;

use OpenAPI\Runtime\ResponseTypes as AbstractResponseTypes;

class ResponseTypes extends AbstractResponseTypes
{
    public array $types = [
        'getAccountCollection' => [
            '200.' => 'LogisticsX\\Finances\\Model\\Account\\Account\\Read[]',
        ],
        'postAccountCollection' => [
            '201.' => 'LogisticsX\\Finances\\Model\\Account\\Account\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getAccountItem' => [
            '200.' => 'LogisticsX\\Finances\\Model\\Account\\Account\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getCurrencyCollection' => [
            '200.' => 'LogisticsX\\Finances\\Model\\Currency\\Currency\\Read[]',
        ],
        'postCurrencyCollection' => [
            '201.' => 'LogisticsX\\Finances\\Model\\Currency\\Currency\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getCurrencyItem' => [
            '200.' => 'LogisticsX\\Finances\\Model\\Currency\\Currency\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putCurrencyItem' => [
            '200.' => 'LogisticsX\\Finances\\Model\\Currency\\Currency\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getStatementDetailCollection' => [
            '200.' => 'LogisticsX\\Finances\\Model\\StatementDetail\\StatementDetail\\Read[]',
        ],
        'postStatementDetailCollection' => [
            '201.' => 'LogisticsX\\Finances\\Model\\StatementDetail\\StatementDetail\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getStatementDetailItem' => [
            '200.' => 'LogisticsX\\Finances\\Model\\StatementDetail\\StatementDetail\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getStatementReasonCollection' => [
            '200.' => 'LogisticsX\\Finances\\Model\\StatementReason\\StatementReason\\Read[]',
        ],
        'postStatementReasonCollection' => [
            '201.' => 'LogisticsX\\Finances\\Model\\StatementReason\\StatementReason\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getStatementReasonItem' => [
            '200.' => 'LogisticsX\\Finances\\Model\\StatementReason\\StatementReason\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putStatementReasonItem' => [
            '200.' => 'LogisticsX\\Finances\\Model\\StatementReason\\StatementReason\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteStatementReasonItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getStatementCollection' => [
            '200.' => 'LogisticsX\\Finances\\Model\\Statement\\Statement\\Read[]',
        ],
        'postStatementCollection' => [
            '201.' => 'LogisticsX\\Finances\\Model\\Statement\\Statement\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getStatementItem' => [
            '200.' => 'LogisticsX\\Finances\\Model\\Statement\\Statement\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'api_statements_statement_detail_get_subresourceStatementSubresource' => [
            '200.' => 'LogisticsX\\Finances\\Model\\StatementDetail\\StatementDetail\\Read',
        ],
        'getUserCollection' => [
            '200.' => 'LogisticsX\\Finances\\Model\\User[]',
        ],
        'postUserCollection' => [
            '201.' => 'LogisticsX\\Finances\\Model\\User',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getUserItem' => [
            '200.' => 'LogisticsX\\Finances\\Model\\User',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putUserItem' => [
            '200.' => 'LogisticsX\\Finances\\Model\\User',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteUserItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'patchUserItem' => [
            '200.' => 'LogisticsX\\Finances\\Model\\User',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
    ];
}
